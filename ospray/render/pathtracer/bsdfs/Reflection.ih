// ======================================================================== //
// Copyright 2009-2015 Intel Corporation                                    //
//                                                                          //
// Licensed under the Apache License, Version 2.0 (the "License");          //
// you may not use this file except in compliance with the License.         //
// You may obtain a copy of the License at                                  //
//                                                                          //
//     http://www.apache.org/licenses/LICENSE-2.0                           //
//                                                                          //
// Unless required by applicable law or agreed to in writing, software      //
// distributed under the License is distributed on an "AS IS" BASIS,        //
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. //
// See the License for the specific language governing permissions and      //
// limitations under the License.                                           //
// ======================================================================== //

#pragma once

#include "BSDF.ih"

struct Reflection
{
  BSDF super;

  vec3f reflectance;
};

inline vec3f Reflection_eval(const varying BSDF* uniform super,
                             const vec3f& wo, const vec3f& wi, float& pdf)
{
  return make_vec3f(0.0f);
}

inline vec3f Reflection_sample(const varying BSDF* uniform super,
                               const vec3f& wo, vec3f& wi, float& pdf, BSDFType& type,
                               const vec2f& s, float ss)
{
  const varying Reflection* uniform self = (const varying Reflection* uniform)super;

  wi = reflect(wo, getN(super));
  pdf = 1.0f;
  type = BSDF_SPECULAR_REFLECTION;
  return self->reflectance;
}

inline void Reflection_Constructor(varying Reflection* uniform self, const varying linear3f* uniform frame,
                                   vec3f reflectance)
{
  BSDF_Constructor(&self->super, BSDF_SPECULAR_REFLECTION,
                   Reflection_eval, Reflection_sample,
                   frame);
  self->reflectance = reflectance;
}

inline varying BSDF* uniform Reflection_create(uniform ShadingContext* uniform ctx, const varying linear3f* uniform frame,
                                               vec3f reflectance)
{
  varying Reflection* uniform self = (varying Reflection* uniform)ShadingContext_alloc(ctx, sizeof(Reflection));
  Reflection_Constructor(self, frame, reflectance);
  return &self->super;
}
